#!/bin/python3

from lib.transformVr import transformVr
from lib.transformConsors import transformConsors
from lib.transformN26 import transformN26
import argparse
import os

parser = argparse.ArgumentParser(description=
    'Transform downloaded transactions csv from banks to YNAB format')
parser.add_argument('-vr', '--Volksbank', dest='vrCsv', required=False,
    help='Path to the Volksbank csv')
parser.add_argument('-c', '--Consorsbank', dest='consorsCsv', required=False,
    help='Path to the Consorsbank csv')
parser.add_argument('-n', '--N26', dest='n26Csv', required=False,
    help='Path to the N26 csv')
parser.add_argument('-o', '--out', dest='outDir', required=True)
args = parser.parse_args()

if(not os.path.isdir(args.outDir)):
    os.makedirs(args.outDir)

if(not args.vrCsv is None):
    transformVr(args.vrCsv, os.path.join(args.outDir, 'vrYNAB.csv'))

if(not args.consorsCsv is None):
    transformConsors(args.consorsCsv, os.path.join(args.outDir, 'consorsYNAB.csv'))

if(not args.n26Csv is None):
    transformN26(args.n26Csv, os.path.join(args.outDir, 'n26YNAB.csv'))
