This script allows converting bank transaction csv files to csv files that can be read by [YNAB](https://www.youneedabudget.com/).

## Supported banks:
- Consorsbank
- Volksbank (tested with one specific Volksbank)
- N26

## Needed dependencies:
- [Python 3](https://www.python.org/)

## Usage
1. Open terminal in the folder of transformCsv.py
2. Call `python ./transformCsv.py` with the appropriate parameters

```
transformCsv.py [-h] [-vr VRCSV] [-c CONSORSCSV] -o OUTDIR

Transform downloaded transactions csv from banks to YNAB format

optional arguments:
  -h, --help            show this help message and exit
  -vr VRCSV, --Volksbank VRCSV
                        Path to the Volksbank csv
  -c CONSORSCSV, --Consorsbank CONSORSCSV
                        Path to the Consorsbank csv
  -n N26CSV, --N26 N26CSV
                        Path to the (german) N26 csv
  -o OUTDIR, --Out OUTDIR
```

Example for a Volksbank Csv:
```
python ./transformCsv.py -vr /path/to/csv.csv -o /path/to/output/directory
```
