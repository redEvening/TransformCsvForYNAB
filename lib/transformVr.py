import csv

def transformVr(path, outpath):
    file = open(path, encoding='iso8859-15')
    out = open(outpath, "w", newline="\n", encoding="utf-8")
    out.seek(0)
    out.truncate()
    reader = csv.reader(file, delimiter = ';')
    writer = csv.writer(out,  dialect="excel")
    writer.writerow(['Date', 'Payee', 'Category', 'Memo', 'Outflow', 'Inflow'])

    # First rows are irrelevant
    for _ in range(1, 17):
        reader.__next__()

    for row in reader:
        if(str(''.join(row)) == ''):
            break

        inflow = row[-1] == 'H'
        date = row[0]
        payee = row[4]
        category = ''
        # memo = str(row[8]).replace('\n', " ")
        memo = ''
        amount = row[-2]

        if(inflow):
            writer.writerow([date, payee, category, memo, 0, amount])
        else:
            writer.writerow([date, payee, category, memo, amount, 0])
                

    out.close()
    print("Output:")
    out = open(outpath, "r")
    reader = csv.reader(out,)
    for row in reader:
        print(row)
