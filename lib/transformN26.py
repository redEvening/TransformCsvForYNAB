import csv
import re
import datetime

def transformN26(path, outpath):
    file = open(path)
    out = open(outpath, "w", newline="\n", encoding="utf-8")
    out.seek(0)
    out.truncate()
    reader = csv.reader(file, delimiter = ',')
    writer = csv.writer(out,  dialect="excel")
    writer.writerow(['Date', 'Payee', 'Category', 'Memo', 'Outflow', 'Inflow'])

    # First rows are irrelevant
    for _ in range(1, 2):
        reader.__next__()

    for row in reader:
        if(str(row) == '[]'):
            break

        date = datetime.datetime.strptime(row[0], "%Y-%m-%d").strftime("%d.%m.%Y")
        payee = row[1]
        category = ''
        memo = ''
        amount = row[-4]
        inflow = (float(amount) >= 0)

        # do not consider internal movements
        if(re.match(r"Von [a-zA-Z\s]+ nach [a-zA-Z\s]+", payee)):
            continue

        if(inflow):
            writer.writerow([date, payee, category, memo, 0, amount])
        else:
            writer.writerow([date, payee, category, memo, -float(amount), 0])
                

    out.close()
    print("Output:")
    out = open(outpath, "r")
    reader = csv.reader(out,)
    for row in reader:
        print(row)
