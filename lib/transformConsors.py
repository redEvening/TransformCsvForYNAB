import csv

def transformConsors(path, outpath):
    print("Transforming Consors")

    file = open(path)
    out = open(outpath, "w", newline="\n", encoding="utf-8")
    out.seek(0)
    out.truncate()
    reader = csv.reader(file, delimiter = ';')
    writer = csv.writer(out,  dialect="excel")
    writer.writerow(['Date', 'Payee', 'Category', 'Memo', 'Outflow', 'Inflow'])

    print("Input:")

    # First line is not relevant
    reader.__next__()

    for row in reader:
        if(str(row) == '[]'):
            break

        date = row[0]
        payee = row[2]
        category = ''
        #   memo = str(row[8]).replace('\n', " ")
        memo = ''
        amountStr = row[-1]
        amount = float(row[-1].replace(",", "."))

        isInflow = not ("-" in amountStr)

        if(isInflow):
            writer.writerow([date, payee, category, memo, 0, amount])
        else:
            # outflow is positive
            amount = amount * (-1)
            writer.writerow([date, payee, category, memo, amount, 0])              

    out.close()
    print("Output:")
    out = open(outpath, "r")
    reader = csv.reader(out,)
    for row in reader:
        print(row)